# Extranet



## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.sorbonne-paris-nord.fr/webmicaa/extranet.git
git branch -M main
git push -uf origin main
```



## Deploy

1. **Prerequisites**:
   - Web server (e.g., Apache, Nginx)
   - Installed PHP

2. **Installation**:
   - Download the source code of the site from the GitHub repository.
   - Place the files in the root directory of your web server.

3. **Startup**:
   - Start your web server.
   - Access the site by opening a browser and entering the URL corresponding to your server.

   Example: http://localhost/my-site/index.php
   
Ensure the path matches where you placed the site files on your server.s

***


## WebMicaa
Why we choose WebMicaa?
The Web, because we do web development. Then Micaa was found with the first letters of the members' first names: M for Marya, I for India, C for Chami (Bilal's surname because the B sounded wrong) and the A's for Aboubakar and Allan.

## Description
Our team is, as you'd expect, particularly development-oriented with Marya and Allan. Despite this, the team is well-balanced by the communication and management strengths of India and Bilal. We can add to this a logical, mathematical mind and a good knowledge of networks and operating systems on the part of Marya and Aboubakar. India will add a little creativity where necessary. 
